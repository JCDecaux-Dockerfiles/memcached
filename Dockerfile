FROM jcdecaux/rhel:7
MAINTAINER Nicolas Mallet <nicolas.mallet@jcdecaux.com>

RUN yum install supervisor memcached -y \
  && yum clean all

COPY supervisord.conf /etc/supervisord.conf

EXPOSE 11211

CMD ["/usr/bin/supervisord"]
